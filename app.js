process.on('unhandledRejection', (reason) => {
	console.log('Error:\n', reason);
});
process.on('test', () => {
	console.log('Error:\n');
});

(async (app) => {
	app.tools = require('osmium-tools');
	app.crypto = require('crypto');
	app.DB = require('osmium-db');
	app.db = new app.DB('admin', 'app', 'PackDX123');
	app.db.disableVirtualRemove();

	require('./webserver')(app);
	await (require('./db'))(app);
	await (require('./backend'))(app);
})({});