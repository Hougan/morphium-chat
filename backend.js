const WebApiServer = require('osmium-webapi').WebApiServer;
const sha256 = require('sha256');

module.exports = async (app) => {
	const server = new WebApiServer(app.io);
	app.hashPass = (user, pass) => sha256(`asfglaksdngalskdgnalksdng:${user}:${pass}`);

	server.registerMiddlewareInc((packet, socket, before) => {
		if (!before) return packet;
		let isOk = false;

		app.tools.iterate(['connect check', 'user check login', 'user register', 'user login', 'user check invite'], (row) => {
			if (packet.name.toLocaleLowerCase() === row) isOk = true;
		});

		if (!isOk && (!socket.handshake.session || !socket.handshake.session.user || !socket.handshake.session.user.login)) return null;
		if (packet.args.length > 1) return null;
		if (packet.args.length === 0) packet.args.push(false);
		packet.args.push(socket.handshake.session);
		return packet;
	});

	app.tools.iterate(['panel', 'login'], (row) => {
		require(`./backend/${row}`)(app, server);
	});
	server.on('connect check', async () => {
		await app.tools.delay(800);
		return 'okay';
	});
};