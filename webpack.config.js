const path = require('path');
const webpack = require('webpack');
const getPath = str => path.resolve(__dirname, str);
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const WebpackShellPlugin = require('webpack-shell-plugin');
const RemoveStrictPlugin = require( 'remove-strict-webpack-plugin' );

module.exports = {
	entry      : {
		index   : './frontend/index.js',
		login   : './frontend/login.js',
		register: './frontend/register.js'
	},
	output     : {
		path      : getPath('public/assets/bundles'),
		publicPath: '/public',
		filename  : '[name].js'
	},
	resolve    : {
		extensions: ['.js', '.vue'],
		alias     : {
			'vue$'  : 'vue/dist/vue.esm.js',
			'public': getPath('./public')
		}
	},
	module     : {
		rules: [
			{
				test   : /\.vue$/,
				loader : 'vue-loader',
				options: {
					loaders: {
						'js'  : 'babel-loader',
						'scss': 'vue-style-loader!css-loader!sass-loader',
						'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
					}
				}
			},
			{
				test  : /\.scss$/,
				loader: ['vue-style-loader', 'css-loader', 'sass-loader']
			},
			{
				test   : /\.js$/,
				use    : {
					loader : 'babel-loader',
					options: {
						plugins: [["module:fast-async", {
							"compiler": {
								"promises"  : true,
								"generators": false
							}
						}]],
						presets: [[
							"@babel/env",
							{"exclude": ["transform-regenerator", "transform-async-to-generator"]}
						]]
					}
				},
				exclude: /node_modules\/(?!(osmium-tools|osmium-events|osmium-webapi)\/).*/
			},
			{
				test  : /\.(png|jpg|gif|svg)$/,
				loader: 'file-loader'
			},
			{
				test  : /\.css$/,
				loader: ['vue-style-loader', 'style-loader', 'css-loader']
			},
			{
				test  : /\.styl$/,
				loader: ['style-loader', 'css-loader', 'stylus-loader']
			},
			{
				test  : /\.pug$/,
				loader: ['pug-plain-loader']
			},
			{
				test: /\.(ttf|eot|woff|woff2)$/,
				use : {
					loader : "file-loader",
					options: {
						name      : "/assets/fonts/[name].[ext]",
						outputPath: '../../'
					}
				}
			}
		]
	},
	devServer  : {
		historyApiFallback: true,
		noInfo            : true
	},
	performance: {
		hints: false
	},
	devtool    : '#eval-source-map',
	plugins    : [
		new VueLoaderPlugin(),
		new WebpackShellPlugin({onBuildStart:['node app.js']})
	]
};


if (process.env.NODE_ENV === 'production') {
	module.exports.devtool = '#source-map';
	module.exports.optimization = {
		minimize : true,
		minimizer: [
			new UglifyJsPlugin({
				sourceMap    : false,
				exclude      : [/(node_modules)/],
				uglifyOptions: {
					mangle  : false,
					warnings: false,
					compress: {
						warnings: false
					},
					output  : {
						comments: false
					},
					parallel: 1
				}
			})
		]
	};
	module.exports.plugins = (module.exports.plugins || []).concat([
		new RemoveStrictPlugin(),
		new webpack.DefinePlugin({
			'process.env': {NODE_ENV: '"production"'}
		}),
		new webpack.LoaderOptionsPlugin({minimize: true})
	]);
}
