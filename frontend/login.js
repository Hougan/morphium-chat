import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import App from './vue/Login.vue';
import '@babel/polyfill';
import WebApi from 'osmium-webapi';
import IO from 'socket.io-client';

Vue.use(Vuetify, {
	theme: {
		primary  : '#54c8f9',
		secondary: '#b0bec5',
		accent   : '#b8d2ff',
		error    : '#770303'
	}
});

new Vue({
	el    : '#app',
	render: h => h(App),
	async mounted() {
		const webApi = new WebApi.WebApiClient(IO('/'));
		window.webApi=webApi;
		await webApi.ready();
		if (await webApi.emit('connect check') !== 'okay') return;
		this.$children[0].webApi = webApi;
		this.$children[0].loginShow = true;
	}
});