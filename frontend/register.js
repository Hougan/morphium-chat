import 'vuetify/dist/vuetify.min.css';
import '@babel/polyfill';
import Vue from 'vue';
import Vuetify from 'vuetify';
import App from './vue/Register.vue';
import IO from 'socket.io-client';
import WebApi from 'osmium-webapi';

Vue.use(Vuetify, {
	theme: {
		primary  : '#045577',
		secondary: '#b0bec5',
		accent   : '#b8d2ff',
		error    : '#770303'
	}
});

new Vue({
	el    : '#app',
	render: h => h(App),
	async mounted() {
		const webApi = new WebApi.WebApiClient(IO('/'));
		window.webApi=webApi;
		await webApi.ready();
		if (await webApi.emit('connect check') !== 'okay') return;
		this.$children[0].webApi = webApi;
		this.$children[0].registerShow = true;
	}
});