import 'vuetify/dist/vuetify.min.css';
import '@babel/polyfill';

import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import '@mdi/font/css/materialdesignicons.min.css';
import IO from 'socket.io-client';
import WebApi from 'osmium-webapi';
import tools from 'osmium-tools';


Vue.use(VueRouter);
Vue.use(Vuetify, {
	iconformat: 'mdi'
});

window.tools = tools;

window.webApi = new WebApi.WebApiClient(IO('/'));

import App from './vue/App.vue';
import Panel from './vue/Panel.vue';

function findInRouter(path) {
	let ret = tools.iterate(router.options.routes, (row) => {
		if (row.path.toLowerCase() !== path.toLowerCase()) return;
		return row;
	}, []);
	return ret[0] ? ret[0] : '';
}

let router = window.router = new VueRouter({
	mode  : 'history',
	routes: [{
		path     : '/panel',
		component: Panel,
		name     : 'Серверы',
		stack    : []
	}, {
		path    : '*',
		redirect: '/panel'
	}]
});

router.panel = {stack: []};
router.afterEach(async (to) => {
	let stack = [];

	await tools.iterate(findInRouter(to.matched[0].path).stack, async (row) => {
		let fpath = row;
		tools.iterate(to.params, (pval, pidx) => {
			fpath = fpath.replace(`:${pidx}`, pval);
		});
		let paramHandler = findInRouter(row).paramHandler;
		if (paramHandler) {
			await tools.iterate(paramHandler, async (fn, id) => {
				stack.push({name: await fn(to.params, id), href: ''});
			});
		}
		stack.push({name: findInRouter(row).name, href: fpath});
	});

	let ph = findInRouter(to.matched[0].path).paramHandler;
	if (ph) await tools.iterate(ph, async (fn, id) => {
		if (!to.params[id]) return;
		stack.push({name: await fn(to.params), href: ''});
	});
	stack.push({name: to.name, path: to.path});
	router.panel.stack = stack;
});

new Vue({
	el    : '#app',
	router,
	render: h => h(App),
	async mounted() {
		await window.webApi.ready();
		this.$children[0].webApi = window.webApi;
		if (this.$children[0].webApiReady) this.$children[0].webApiReady();
	}
});
