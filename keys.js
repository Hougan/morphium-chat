#!/usr/bin/env node
process.on('unhandledRejection', (reason) => {
	console.log('Error:\n', reason);
});
process.on('test', () => {
	console.log('Error:\n');
});

(async (app) => {
	app.tools = require('osmium-tools');
	app.crypto = require('crypto');
	app.DB = require('osmium-db');
	app.argv = require('optimist').argv;
	app.db = new app.DB('admin', 'app', 'PackDX123');
	app.db.disableVirtualRemove();

	function genCode(no) {
		const shasum = app.crypto.createHash('sha512');
		shasum.update(`azimuthInvite${no}Vjsk19Dn`);
		return shasum.digest('base64').replace(/[+=\/]/g, '').toUpperCase().substr(0, 12) + parseInt(no).pad(5);
	}

	await (require('./db'))(app);
	if (app.argv._.length === 0) {
		console.log('No specific args found! Usage: keys.js <amount> to generate keys!');
	}
	else {
		for (let i = 0; i < app.argv._[0]; i++) {
			var lastId = await app.db.models.invites.findOne({order: [['no', 'DESC']]});
			lastId = lastId ? lastId.no : 1;
			console.log(lastId);

			await app.db.models.invites.create({code: genCode(lastId + 1), no: lastId + 1, used: false});
		}
		console.log(`Genereated ${app.argv._[0]} codes!`);
	}
})({});