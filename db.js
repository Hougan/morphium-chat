module.exports = async function (app) {
	app.db.defineSchema({
		'users'   : {
			login      : 'string',
			password   : 'string',
			admin      : 'boolean',
			ignore     : 'boolean',
			data       : 'jsonb',
			'>chat'    : {
				message: 'text',
				ip     : 'string'
			},
			'>settings': {
				globalChat : 'boolean',
				soundGlobal: 'boolean',
				soundLocal : 'boolean'
			},
			'>ignores' : {
				ignoreId: 'integer',
				ignore  : 'boolean'
			}
		},
		'invites' : {
			code: 'string',
			no  : 'integer',
			used: 'boolean'
		},
		'connects': {
			ip    : 'string',
			amount: 'integer'
		}
	});

	await app.db.sync({force: false});
};