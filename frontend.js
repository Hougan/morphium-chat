import IO from 'socket.io-client';
import WebApi from 'osmium-webapi';

(async () => {
	const webApi = new WebApi.WebApiClient(IO('/'));
	await webApi.ready();

	console.log(await webApi.emit('add message', {
		message: 'Привет куку',
		from   : 'Ололо'
	}));

})();
