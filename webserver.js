const session = require('express-session');
const sessionIo = require("express-socket.io-session");

module.exports = function (app) {
	async function render(res, name) {
		res.end(await app.fs.readFile(`views/${name}`));
	}

	app.Express = require('express');
	const cookieParser = require('cookie-parser');
	const bodyParser = require('body-parser');
	app.fs = require('mz/fs');

	app.express = app.Express();
	app.session = session;

	app.express.use(bodyParser.json());
	app.express.use(bodyParser.urlencoded({extended: true}));
	app.express.use(cookieParser());
	app.express.set('trust proxy', 1);
	app.express.set('view engine', 'pug');
	app.express.set('views', './views');

	app.express.use(app.Express.static(`${__dirname}/public`));

	app.httpServer = require('http').Server(app.express);
	app.io = require('socket.io')(app.httpServer);

	app.sessionStore = new (require('connect-session-sequelize')(session.Store))({
		db                     : app.db,
		checkExpirationInterval: 900000,
		expiration             : 15552000000
	});

	const expressSession = app.session({
		store            : app.sessionStore,
		secret           : "8c2651Ar3xgRm1iwWQryPiFeakrjVoFvFTkxRHg0",
		name             : "sid",
		resave           : false,
		proxy            : true,
		saveUninitialized: true
	});
	app.express.use(expressSession);
	app.io.use(sessionIo(expressSession, {autoSave: true}));

	app.sessionStore.sync();

	require('./routes')(app);


	//Запуск веб-сервера
	app.httpServer.listen(8089);
};