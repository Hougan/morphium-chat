module.exports = async (app, server) => {
	server.on('user login', async (data) => {
		let password = app.hashPass(data.login, data.password);
		let ret = await app.db.models.users.findOne({
			include   : [{
				model     : app.db.models.settings,
				required  : true,
				attributes: ['globalChat', 'soundGlobal', 'soundLocal']
			}, {
				model     : app.db.models.ignores,
				required  : false,
				attributes: ['ignore', 'ignoreId']
			}],
			where     : { login: data.login, password },
			attributes: ['id', 'login', 'admin', 'ignore', 'data', 'createdAt', 'updatedAt']
		});
		if (!ret) return false;

		let token = app.tools.GUID();
		app.tempTokens[token] = ret.dataValues;
		return token;
	});



	function genCode(no) {
		const shasum = app.crypto.createHash('sha512');
		shasum.update(`azimuthInvite${no}Vjsk19Dn`);
		return shasum.digest('base64').replace(/[+=\/]/g, '').toUpperCase().substr(0, 12) + no.pad(5);
	}

	function getCode(code) {
		if (code.length !== 17) return false;
		let no = parseInt(code.substr(12, 5));
		if (genCode(no) !== code) return false;
		return no;
	}

	function getProductCode(code) {
		switch (parseInt(code.substr(12, 1))) {
			case 5:
			case 6:
				return 3; //База плагинов
			case 7:
			case 8:
				return 2; //Анализатор
			case 9:
				return 4; //Резерв
			default:
				return 1; //Хостинг (1-3)
		}
	}

	server.on('user check invite', async (data) => {
		let no = getCode(data.invite);
		if (!no) return false;

		let res = await app.db.models.invites.findOne({where: {no}});
		if (!res) return false;
		if (res.used) return null;

		return true;
	});

	const UserCheckLogin = async (data) => {
		return !await app.db.models.users.findOne({where: {login: data.login}});
	};

	server.on('user check login', UserCheckLogin);

	server.on('user register', async (data) => {
		let no = getCode(data.invite);
		if (!no) return {result: 'error', msg: 'Неверный код приглашения'};

		let res = await app.db.models.invites.findOne({where: {no, used: false}});
		if (!res) return {result: 'error', msg: 'Код приглашения не найден или зарегистрирован'};

		if (!await UserCheckLogin({login: data.login})) return {result: 'error', msg: 'Пользователь уже зарегистрирован'};

		let cret = await app.db.models.users.create({
			login   : data.login,
			password: app.hashPass(data.login, data.password),
			type    : 1,
			admin   : false,
			ignore  : false,
			data    : {
				inviteNo: no
			}
		});
		if (!cret) return {result: 'error', msg: 'Ошибка регистрации'};
		await app.db.models.settings.create({
			globalChat : true,
			soundGlobal: true,
			soundLocal : true,
			userId     : cret.id
		});

		await app.db.models.invites.update({used: true}, {where: {no}});

		return {result: 'ok'};
	});

};