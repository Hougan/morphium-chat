module.exports = (app, server) => {
	let cooldowns = {};
	let currentTime = () => new Date().toLocaleTimeString();
	let messagesFetch = async () => {
		let response = await app.db.models.chat.findAll({
			limit  : 10,
			include: [{
				model     : app.db.models.users,
				required  : true,
				attributes: ['login', 'admin']
			}],
			order  : [
				['id', 'DESC']
			]
		});
		return response ? response : [];
	};
	console.log(`[${currentTime()}] Server started`);

	server.on('messages fetch', async () => {
		return {history: await messagesFetch()};
	});
	server.on('credentials get', async (_, session) => {
		return {user: session.user};
	});
	server.on('credentials update', async (user, session) => {
		session.user = user;
		session.save((err) => {
			if (err) {
				console.log('Failed re-update session for user: ', user.login);
			}
		});
		let set = await app.db.models.settings.update({
				globalChat : user.settings.globalChat,
				soundGlobal: user.settings.soundGlobal,
				soundLocal : user.settings.soundLocal
			},
			{where: {userId: user.id}}
		);

		let objArr = Object.keys(user.ignores);
		for (let i = 0; i < objArr.length; i++) {
			let where = {userId: user.id, ignoreId: Object.keys(user.ignores)[i]};
			let res = await app.db.models.ignores.findOne({ where });
			if (!res){
				where.ignore = user.ignores[Object.keys(user.ignores)[i]];
				await app.db.models.ignores.create(where);
			}
			else{
				await app.db.models.ignores.update({ignore: user.ignores[Object.keys(user.ignores)[i]]}, {where})
			}
		}
		return set ? 'OK' : 'ERROR';
	});

	async function getNormalData() {
		let rightData = [];
		for (let i = 0; i < allClients.length; i++) {
			let session = allClients[i].handshake.session;
			rightData.push({id: session.user.id, login: session.user.login});
		}
		return rightData;
	}

	server.on('connects update', async () => {
		return await getNormalData();
	});
	server.on('connect kick', (con, session) => {
		if (!session.user.admin) return;
		for (let i = 0; i < allClients.length; i++){
			if (allClients[i].handshake.session.user.id === con.id){
				console.log(allClients[i].handshake.session.user.login + " was kicked by " + session.user.login);
				allClients[i].disconnect();
				return;
			}
		}
	});


	server.on('message send', async (msg, session) => {
		if (cooldowns[session.user.login]) return;

		msg.user = session.user;
		msg.userId = session.user.id;

		msg.id = (await app.db.models.chat.create(msg)).id;
		server.emit('message on send', msg);

		cooldowns[session.user.login] = true;
		setTimeout(() => {
			delete cooldowns[session.user.login];
		}, 2000);
	});
	server.on('message remove', async (msg, session) => {
		if (!session.user.admin && msg.user.login !== session.user.login) return;
		await app.db.models.chat.destroy({
			where: {
				id: msg.id
			}
		});
		server.emit('message on remove', msg);
	});

	var allClients = [];
	app.io.sockets.on('connection', async function (socket) {
		console.log(`[${currentTime()}] ${socket.handshake.session.user.login} connected`);
		allClients.push(socket);

		let ret = await app.db.models.users.findOne({
			include   : [{
				model     : app.db.models.settings,
				required  : true,
				attributes: ['globalChat', 'soundGlobal', 'soundLocal']
			}, {
				model     : app.db.models.ignores,
				required  : false,
				attributes: ['ignore', 'ignoreId']
			}],
			where     : {login: socket.handshake.session.user.login},
			attributes: ['id', 'login', 'admin', 'ignore', 'data', 'createdAt', 'updatedAt']
		});
		if (!ret) {
			console.log('Error reading session for user: ', socket.handshake.session.user.login);
			return;
		}
		socket.handshake.session.user = ret.dataValues;
		socket.handshake.session.save(function (err) {
			if (err) console.log('Error saving session for: ', socket.handshake.session.user.login);
		});

		server.emit('connects update', await getNormalData());
		socket.on('disconnect', async function () {
			console.log(`[${currentTime()}] ${socket.handshake.session.user.login} disconnected`);

			var i = allClients.indexOf(socket);
			allClients.splice(i, 1);
			server.emit('connects update', await getNormalData());
		});
	});
};