module.exports = (app) => {
	//Роутер
	app.checkAuth = (req, res, next) => req.session.user ? next() : res.redirect('/login');
	app.tempTokens = {};

	app.express.get('/register', async (req, res) => res.render('register'));
	app.express.get('/login', async (req, res) => res.render('login'));

	app.express.get('/login/process', async (req, res) => {
		if (!req.query.token) return res.redirect('/login');
		let data = app.tempTokens[req.query.token];
		if (!data) return res.redirect('/login');

		req.session.user = data;
		delete app.tempTokens[req.query.token];
		return res.redirect('/');
	});

	app.express.get('/logout', async (req, res) => {
		req.session.user = false;
		return res.redirect('/');
	});

	app.express.post('/api/send', async (req, res) => {
		JSON.parse(req.body.payload);
		res.json({status:'OK '});
	});

	app.express.get('*', app.checkAuth, async (req, res) => {
		res.render('index');
	});
};